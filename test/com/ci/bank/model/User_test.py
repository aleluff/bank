# -- main.com.ci.bank.model --

import unittest
from main.com.ci.bank.model.User import User

class User_test(unittest.TestCase):
    userTmp = ""

    @classmethod
    def setUpClass(cls):
        """Preparing the current TestClass, launch one time before the TestClass start"""
        print("\n--Preparing the User_Test class\n\n")
        cls.userTmp = User("nom", "prenom", "login", "pwd", "adresse")
    
    @classmethod
    def tearDownClass(cls):
        """Clearing the current TestClass, launch one time after the TestClass end"""
        print("--Clearing the current TestClass \n")
        cls.userTmp = None

    def setUp(self):
        """ Preparing current test method, launch before each test function"""
        print("------Preparing the current test method")
        self.userTmp = User("nom", "prenom", "login", "pwd", "adresse")

    def tearDown(self):
        """ Cleaning current test method results, launch after each test function"""
        print("------Cleaning the current test method \n")

    def test_creation_user(self):
        print("------------ test_creation_user TEST")
        self.assertIsNotNone(self.userTmp)

    def test_getters(self):
        print("------------ test_getters TEST")
        self.assertEqual(self.userTmp.m_id, self.userTmp.getId(), "id getter error")
        self.assertEqual(self.userTmp.m_nom, self.userTmp.getNom(), "nom getter error")
        self.assertEqual(self.userTmp.m_prenom, self.userTmp.getPrenom(), " prenom getter error")
        self.assertEqual(self.userTmp.m_login, self.userTmp.getLogin(), "login getter error")
        self.assertEqual(self.userTmp.m_pwd, self.userTmp.getPwd(), " password getter error")
        self.assertEqual(self.userTmp.m_adresse, self.userTmp.getAdresse(), "adresse getter error")

    def test_all_params(self):
        print("------------ test_all_params TEST")
        self.assertEqual(self.userTmp.m_nom, "nom", " nom not set correctly")
        self.assertEqual(self.userTmp.m_prenom, "prenom", " prenom not set correctly")
        self.assertEqual(self.userTmp.m_login, "login", " login not set correctly")
        self.assertEqual(self.userTmp.m_pwd, "pwd", " password not set correctly")
        self.assertEqual(self.userTmp.m_adresse, "adresse", " adresse not set correctly")

    def test_setter_user(self):
        print("------------ test_setter_user TEST")
        self.userTmp.setNom("nouveaunom")
        self.userTmp.setPrenom("nouveauprenom")
        self.userTmp.setLogin("nouveaulogin")
        self.userTmp.setPwd("nouveaupwd")
        self.userTmp.setAdresse("nouvelleadresse")
        self.assertEqual(self.userTmp.m_nom, "nouveaunom", " nom not set correctly")
        self.assertEqual(self.userTmp.m_prenom, "nouveauprenom", " prenom not set correctly")
        self.assertEqual(self.userTmp.m_login, "nouveaulogin", " login not set correctly")
        self.assertEqual(self.userTmp.m_pwd, "nouveaupwd", " password not set correctly")
        self.assertEqual(self.userTmp.m_adresse, "nouvelleadresse", " adresse not set correctly")

    def test_auth_user(self):
        print("------------ test_auth_user TEST")
        self.assertTrue(self.userTmp.auth("pwd"))
        self.assertFalse(self.userTmp.auth("password"))

    def test_checkLogin_user(self):
        print("------------ test_checkLogin_user TEST")
        self.assertTrue(self.userTmp.checkLogin("login"))
        self.assertFalse(self.userTmp.checkLogin("logine"))