# -- test.com.ci.bank.model --

import unittest, json
from main.com.ci.bank.model.Accounting import Accounting
from main.com.ci.bank.model.AccountingUnit import AccountingUnit

class Accounting_test(unittest.TestCase):
    def setUp(self):
        """ Preparing current test method, launch before each test function"""
        print("------Preparing the current test method")
        self.accountingTmp = Accounting()

    def tearDown(self):
        """ Cleaning current test method results, launch after each test function"""
        print("------Cleaning the current test method \n")

    def test_creation_Accounting(self):
        print("------------ test_creation_Accounting TEST")
        self.assertIsNotNone(self.accountingTmp)

    def test_getters(self):
        print("------------ test_getters TEST")
        self.assertEqual(self.accountingTmp.m_eventList, self.accountingTmp.getEventList(), "event list getter error")

    def test_all_params(self):
        print("------------ test_all_params TEST")
        temp = AccountingUnit("eventName", "eventDetails")
        self.accountingTmp.addEvent(temp)
        temp = AccountingUnit("eventName2", "eventDetails2")
        self.accountingTmp.addEvent(temp)
        self.assertIsNotNone(self.accountingTmp.getEventList()[0])
        self.assertIsNotNone(self.accountingTmp.getEventList()[1])
        self.assertNotEqual(self.accountingTmp.getEventList()[0], temp, "event not set properly")
        self.assertEqual(self.accountingTmp.getEventList()[1], temp, "event not set properly")
