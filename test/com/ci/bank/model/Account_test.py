# -- test.main.com.ci.bank.model --
# -- classe de test Account --

import unittest
from main.com.ci.bank.model.Account import Account
from main.com.ci.bank.model.User import User

class Account_test(unittest.TestCase):
    accountTmp = ""

    @classmethod
    def setUpClass(cls):
        """Preparing the current TestClass, launch one time before the TestClass start"""
        print("\n--Preparing the Account_test class\n\n")
        utilisateur1 = User("nom", "prenom", "login", "pwd", "adresse")
        utilisateur2 = User("nom2", "prenom2", "login2", "pwd2", "adresse2")
        cls.accountTmp = Account(utilisateur1)
        cls.accountTmp.addUser(utilisateur2)
    
    @classmethod
    def tearDownClass(cls):
        """Clearing the current TestClass, launch one time after the TestClass end"""
        print("--Clearing the current TestClass \n")
        cls.accountTmp = None
    
    def setUp(self):
        """ Preparing current test method, launch before each test function"""
        print("------Preparing the current test method")
        utilisateur1 = User("nom", "prenom", "login", "pwd", "adresse")
        utilisateur2 = User("nom2", "prenom2", "login2", "pwd2", "adresse2")
        self.accountTmp = Account(utilisateur1)
        self.accountTmp.addUser(utilisateur2)

    def tearDown(self):
        """ Cleaning current test method results, launch after each test function"""
        print("------Cleaning the current test method \n")

    def test_account_creation(self):
        self.assertIsNotNone(self.accountTmp)
        self.assertIsNotNone(self.accountTmp.m_userList[0])
        self.assertIsNotNone(self.accountTmp.m_userList[1])

    def test_getters(self):
        print("------------ test_getters TEST")
        self.assertEqual(self.accountTmp.m_id, self.accountTmp.getId(), "id getter error")
        self.assertEqual(self.accountTmp.m_solde, self.accountTmp.getSolde(), "solde getter error")
        self.assertEqual(self.accountTmp.m_userList, self.accountTmp.getUserList(), "userList getter error")

    def test_all_params(self):
        print("------------ test_all_params TEST")
        self.assertEqual(self.accountTmp.m_solde, 0, " solde not set correctly")
        self.assertEqual(self.accountTmp.m_userList[0].m_login, "login", " login not set correctly")
        self.assertEqual(self.accountTmp.m_userList[1].m_login, "login2", " login not set correctly")

    def test_debit(self):
        self.accountTmp.m_solde = 100
        self.accountTmp.debit(50)
        self.assertEqual(self.accountTmp.m_solde, 50, " solde pas debité correctly")

    def test_credit(self):
        self.accountTmp.m_solde = 100
        self.accountTmp.credit(50)
        self.assertEqual(self.accountTmp.m_solde, 150, " solde pas credité correctly")

    def test_auth(self):
        self.assertTrue(self.accountTmp.auth("login2", "pwd2"))
        self.assertFalse(self.accountTmp.auth("login", "mauvaispwd"))
        self.assertFalse(self.accountTmp.auth("mauvaislogin", "mauvaispwd"))
        self.assertFalse(self.accountTmp.auth("mauvaislogin", "pwd"))

    def test_adduser(self):
        user3 = User("nom3", "prenom3", "login3", "pwd3", "adresse3")
        self.accountTmp.addUser(user3)
        self.assertIsNotNone(self.accountTmp.m_userList[2])
        self.assertEqual(self.accountTmp.m_userList[2].m_login, "login3", " login of adduser not set correctly")
        
    def test_deluser(self):
        self.accountTmp.delUser("login2")
        self.assertEqual(len(self.accountTmp.m_userList), 1, "deluser not correctly")

