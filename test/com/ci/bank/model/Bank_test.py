#Bank -- main.com.ci.bank.model --

import unittest
from urllib.parse import uses_fragment
from main.com.ci.bank.model.Bank import Bank
from main.com.ci.bank.model.User import User
from main.com.ci.bank.model.Account import Account
from main.com.ci.bank.model.Credit import Credit


class Bank_test(unittest.TestCase):
    bankTmp = ""
    userTmp = User("nom", "prenom", "login", "pwd", "adresse")
    accountTmp = Account(userTmp)

    def setUp(self):
        """ Preparing current test method, launch before each test function"""
        print("------Preparing the current test method")
        self.bankTmp = Bank("mabanque")
        self.userTmp = User("nom", "prenom", "login", "pwd", "adresse")
        self.accountTmp = Account(self.userTmp)
        self.creditTmp = Credit("nomcredit", self.userTmp, 10000, 5)
        self.bankTmp.addAccount(self.accountTmp)
        self.bankTmp.addUser(self.userTmp)
        self.bankTmp.addCredit(self.creditTmp)

    def tearDown(self):
        """ Cleaning current test method results, launch after each test function"""
        print("------Cleaning the current test method \n")

    def test_creation_bank(self):
        print("------------ test_creation_bank TEST")
        self.assertIsNotNone(self.bankTmp)

    def test_getters(self):
        print("------------ test_getters TEST")
        self.assertEqual(self.bankTmp.m_id, self.bankTmp.getId(), "id getter error")
        self.assertEqual(self.bankTmp.m_nom, self.bankTmp.getNom(), "nom getter error")
        self.assertIsNotNone(self.bankTmp.getAccounts())
        self.assertIsNotNone(self.bankTmp.getUsers())
        self.assertIsNotNone(self.bankTmp.getCredits())

    def test_all_params(self):
        print("------------ test_all_params TEST")
        self.assertEqual(self.bankTmp.m_nom, "mabanque", " nom not set correctly")
        self.assertEqual(self.bankTmp.getUsers()[0], self.userTmp, " user not set correctly")
        self.assertEqual(self.bankTmp.getAccounts()[0], self.accountTmp, " accounts not set correctly")
        self.assertEqual(self.bankTmp.getCredits()[0], self.creditTmp, " credits not set correctly")
