# -- test.com.ci.bank.model --

import unittest
from datetime import datetime
from main.com.ci.bank.model.Transaction import Transaction
from main.com.ci.bank.model.User import User

class Transaction_test(unittest.TestCase):
    transactionTmp = ""

    def setUp(self):
        """ Preparing current test method, launch before each test function"""
        print("------Preparing the current test method")
        self.transactionTmp = Transaction("débit", 500)

    def tearDown(self):
        """ Cleaning current test method results, launch after each test function"""
        print("------Cleaning the current test method \n")

    def test_creation_Transaction(self):
        print("------------ test_creation_Transaction TEST")
        self.assertIsNotNone(self.transactionTmp)

    def test_getters(self):
        print("------------ test_getters TEST")
        self.assertEqual(self.transactionTmp.m_id, self.transactionTmp.getId(), "id getter error")
        self.assertEqual(self.transactionTmp.m_date, self.transactionTmp.getDate(), "date getter error")
        self.assertEqual(self.transactionTmp.m_type, self.transactionTmp.getType(), "type getter error")
        self.assertEqual(self.transactionTmp.m_montant, self.transactionTmp.getMontant(), "montant getter error")

    def test_all_params(self):
        print("------------ test_all_params TEST")
        self.assertEqual(self.transactionTmp.m_type, "débit", "typetransaction not set correctly")
        self.assertEqual(self.transactionTmp.m_montant, 500, " montant not set correctly")
        madate = datetime.now()
        madateTmp = madate
        self.transactionTmp.calculQuinzaine(madate)
        self.assertNotEqual(self.transactionTmp.getDate(), madateTmp)
        if madateTmp.day > 15:
            self.assertEqual(self.transactionTmp.getDate().day, 1)
            if madateTmp.month == 12:
                self.assertEqual(self.transactionTmp.getDate().month, 1)
                self.assertEqual(self.transactionTmp.getDate().year, madateTmp.year+1)
            else:
                self.assertEqual(self.transactionTmp.getDate().month, madateTmp.month+1)
        else:
            self.assertEqual(self.transactionTmp.getDate().day, 16)