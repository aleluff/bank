# -- test.com.ci.bank.model --

import unittest, json
from main.com.ci.bank.model.AccountingUnit import AccountingUnit

class AccountingUnit_test(unittest.TestCase):
    def setUp(self):
        """ Preparing current test method, launch before each test function"""
        print("------Preparing the current test method")
        self.accountingUnitTmp = AccountingUnit("eventName", "eventDetails")

    def tearDown(self):
        """ Cleaning current test method results, launch after each test function"""
        print("------Cleaning the current test method \n")

    def test_creation_AccountingUnit(self):
        print("------------ test_creation_AccountingUnit TEST")
        self.assertIsNotNone(self.accountingUnitTmp)

    def test_getters(self):
        print("------------ test_getters TEST")
        self.assertEqual(self.accountingUnitTmp.m_id, self.accountingUnitTmp.getId(), "id getter error")
        self.assertEqual(self.accountingUnitTmp.m_date, self.accountingUnitTmp.getDate(), "date getter error")
        self.assertEqual(self.accountingUnitTmp.m_event_name, self.accountingUnitTmp.getEventName(), "event name getter error")
        self.assertEqual(self.accountingUnitTmp.m_detail, self.accountingUnitTmp.getDetail(), "details getter error")

    def test_all_params(self):
        print("------------ test_all_params TEST")
        self.assertEqual(self.accountingUnitTmp.m_event_name, "eventName", "event name not set correctly")
        self.assertEqual(self.accountingUnitTmp.m_detail, "eventDetails", " detail not set correctly")

    def test_toJson(self):
        print("------------ test_toJson TEST")
        self.assertEqual(self.accountingUnitTmp.toJson(), json.dumps(self.accountingUnitTmp.__dict__, indent=4, sort_keys=True, default=str), "json not set correctly")
