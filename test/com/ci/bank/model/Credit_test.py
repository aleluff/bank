# -- test.com.ci.bank.model --

import unittest
from main.com.ci.bank.model.Credit import Credit
from main.com.ci.bank.model.User import User

class Credit_test(unittest.TestCase):
    creditTmp = ""
    utilisateur = User("nom", "prenom", "login", "pwd", "adresse")

    def setUp(self):
        """ Preparing current test method, launch before each test function"""
        print("------Preparing the current test method")
        self.creditTmp = Credit("nomcredit", self.utilisateur, 10000, 5)

    def tearDown(self):
        """ Cleaning current test method results, launch after each test function"""
        print("------Cleaning the current test method \n")

    def test_creation_Credit(self):
        print("------------ test_creation_Credit TEST")
        self.assertIsNotNone(self.creditTmp)

    def test_getters(self):
        print("------------ test_getters TEST")
        self.assertEqual(self.creditTmp.m_id, self.creditTmp.getId(), "id getter error")
        self.assertEqual(self.creditTmp.m_nom, self.creditTmp.getNom(), "nom getter error")
        self.assertEqual(self.creditTmp.m_user, self.creditTmp.getUser(), " user getter error")
        self.assertEqual(self.creditTmp.m_value, self.creditTmp.getValue(), "value getter error")
        self.assertEqual(self.creditTmp.m_interet, self.creditTmp.getInteret(), " interet getter error")

    def test_all_params(self):
        print("------------ test_all_params TEST")
        self.assertEqual(self.creditTmp.m_nom, "nomcredit", " nomcredit not set correctly")
        self.assertEqual(self.creditTmp.m_value, 10000, " valuecredit not set correctly")
        self.assertEqual(self.creditTmp.m_interet, 5, " interetcredit not set correctly")
