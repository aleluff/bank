# -- main.com.ci.bank --
# -- Contient le main de l'application --

from model.User import User
from model.Account import Account
from model.Bank import Bank

bobby = User("johnson", "bobby", "monlogin", "123456", "monadresse")
johnny = User("carlson", "johnny", "pgmdu98", "motdepasse", "paris")
gary = User("benson", "gary", "trolilol", "password", "jerusalem")

comptedegary = Account(gary)
comptedejohnny = Account(johnny)

comptedegary.credit(100)
comptedegary.debit(20)
comptedegary.credit(50)
comptedegary.credit(35)

comptedegary.calculInteret(2026)

comptedegary.addUser(johnny)

caisseEpargne = Bank("Caisse d'épargne")
caisseEpargne.addAccount(comptedegary)
caisseEpargne.addAccount(comptedejohnny)

caisseEpargne.getAccounts()[0].consultation()

caisseEpargne.addUser(gary)
caisseEpargne.addUser(johnny)

caisseEpargne.getUsers()[0].consultation()
caisseEpargne.getUsers()[1].consultation()
