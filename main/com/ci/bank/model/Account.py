# -- main.com.ci.bank.model --
# -- classe Account --

from datetime import datetime
from main.com.ci.bank.model.Transaction import Transaction
import uuid


class Account():
    # -- Constructeur --
    def __init__(self, user):
        self.m_id = uuid.uuid1()
        self.m_userList = []
        self.m_userList.append(user)
        self.m_solde = 0
        self.m_decouvertAutorise = 400
        self.m_dateCreation = datetime.now()
        self.m_tauxInteret = 1.35
        self.m_historiqueTransaction = []

# -- Getter --
    def getId(self):
        return self.m_id

    def getSolde(self):
        return self.m_solde

    def getUserList(self):
        return self.m_userList

    def getTaux(self):
        return self.m_tauxInteret

    def getDecouvert(self):
        return self.m_decouvertAutorise

    def getHistoriqueTransaction(self):
        return self.m_historiqueTransaction

# -- Setter --
    def setTaux(self, nouveauTaux):
        self.m_tauxInteret = nouveauTaux

# -- Méthodes --
    def debit(self, sommeRetrait):
        self.m_solde = self.m_solde - sommeRetrait
        self.m_historiqueTransaction.append(Transaction("Débit", sommeRetrait))

    def credit(self, sommeAjout):
        self.m_solde = self.m_solde + sommeAjout
        self.m_historiqueTransaction.append(Transaction("Crédit", sommeAjout))

    def consultation(self):
        print("\nConsultation de votre compte :")
        print(f"\nIdentifiant : {self.getId()}")
        print(f"Solde : {self.getSolde()}")
        print(f"Taux d'intéret : {self.getTaux()}%")
        print(f"Découvert autorisé : {self.getDecouvert()}")
        print("Utilisateurs : ")
        for user in self.m_userList:
            print(f"- {user.getNom()} {user.getPrenom()}")

    def consulteHistorique(self):
        print("\nHistorique des transactions :")
        for transaction in self.m_historiqueTransaction:
            print(f"\nDate : {transaction.getDate()}")
            print(f"Type : {transaction.getType()}")
            print(f"Montant : {transaction.getMontant()}")

    def auth(self, login, password):
        for user in self.m_userList:
            if user.checkLogin(login):
                return user.auth(password)
        return False

    def addUser(self, user):
        self.m_userList.append(user)

    def delUser(self, userLogin):
        for user in self.m_userList:
            if user.checkLogin(userLogin):
                self.m_userList.remove(user)
                return True
        return False

    def getUsers(self):
        for user in self.m_userList:
            user.consultation()

    def calculInteret(self, anneeActuel):
        print(f"\nIntéret à l'année : {anneeActuel}")
        print(self.m_tauxInteret/100 * self.m_solde * (anneeActuel - self.m_dateCreation.year))
