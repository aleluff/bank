# -- main.com.ci.bank.model --
# -- classe Credit --

import uuid


class Credit():
    # -- Constructeur --
    def __init__(self, nomCredit, user, valueCredit, interet):
        self.m_id = uuid.uuid1()
        self.m_nom = nomCredit
        self.m_user = user
        self.m_value = valueCredit
        self.m_interet = interet

# -- Getter --
    def getId(self):
        return self.m_id

    def getNom(self):
        return self.m_nom

    def getUser(self):
        return self.m_user

    def getValue(self):
        return self.m_value

    def getInteret(self):
        return self.m_interet
