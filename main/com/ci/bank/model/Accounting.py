# -- main.com.ci.bank.model --
# -- classe Accounting --


class Accounting():
    # -- Constructeur --
    def __init__(self):
        self.m_eventList = []

# -- Getter --
    def getEventList(self):
        return self.m_eventList

    def addEvent(self, event):
        self.m_eventList.append(event)

    def showEvents(self):
        print("\nEvents :")
        for event in self.m_eventList:
            print(event.toJson())

    def saveEvents(self, callback):
        string = ""
        for event in self.m_eventList:
            string += event.toJson()
        callback(string)
