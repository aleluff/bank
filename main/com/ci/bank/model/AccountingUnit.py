# -- main.com.ci.bank.model --
# -- classe AccountingUnit --

from datetime import datetime
import json
import uuid


class AccountingUnit():
    compteur = 0

    # -- Constructeur --
    def __init__(self, eventName, detail):
        self.m_id = uuid.uuid1()
        self.m_date = datetime.now()
        self.m_event_name = eventName
        self.m_detail = detail

# -- Getter --
    def getId(self):
        return self.m_id

    def getDate(self):
        return self.m_date

    def getEventName(self):
        return self.m_event_name

    def getDetail(self):
        return self.m_detail

    def toJson(self):
        return json.dumps(self.__dict__, indent=4, sort_keys=True, default=str)
