# -- main.com.ci.bank.model --
# -- classe User --

import uuid


class User():
    # -- Constructeur --
    def __init__(self, nom, prenom, login, pwd, adresse):
        self.m_id = uuid.uuid1()
        self.m_nom = nom
        self.m_prenom = prenom
        self.m_login = login
        self.m_pwd = pwd
        self.m_adresse = adresse

# -- Getter --
    def getId(self):
        return self.m_id

    def getNom(self):
        return self.m_nom

    def getPrenom(self):
        return self.m_prenom

    def getLogin(self):
        return self.m_login

    def getPwd(self):
        return self.m_pwd

    def getAdresse(self):
        return self.m_adresse

# -- Setter --
    def setNom(self, nouveauNom):
        self.m_nom = nouveauNom

    def setPrenom(self, nouveauPrenom):
        self.m_prenom = nouveauPrenom

    def setLogin(self, nouveauLogin):
        self.m_login = nouveauLogin

    def setPwd(self, nouveauPwd):
        self.m_pwd = nouveauPwd

    def setAdresse(self, nouvelleAdresse):
        self.m_adresse = nouvelleAdresse

# -- Méthodes --
    def auth(self, passwordSaisi):
        if self.m_pwd == passwordSaisi:
            return True
        else:
            return False

    def checkLogin(self, loginSaisi):
        if self.m_login == loginSaisi:
            return True
        else:
            return False

    def consultation(self):
        print(f"\nIdentifiant : {self.getId()}")
        print(f"Nom : {self.getNom()}")
        print(f"Prenom : {self.getPrenom()}")
        print(f"Login : {self.getLogin()}\n")
