# -- main.com.ci.bank.model --
# -- classe Transaction --

from datetime import datetime
import uuid


class Transaction():
    # -- Constructeur --
    def __init__(self, typeTransaction, montantTransaction):
        self.m_id = uuid.uuid1()
        self.m_date = self.calculQuinzaine(datetime.now())
        self.m_type = typeTransaction
        self.m_montant = montantTransaction

# -- Getter --
    def getId(self):
        return self.m_id

    def getDate(self):
        return self.m_date

    def getType(self):
        return self.m_type

    def getMontant(self):
        return self.m_montant

    def calculQuinzaine(self, date):
        if date.day > 15:
            if date.month == 12:
                return date.replace(day=1, month=1, year=date.year+1)
            else:
                return date.replace(day=1, month=date.month+1)
        else:
            return date.replace(day=16)
