# -- main.com.ci.bank.model --
# -- classe Bank --

import uuid


class Bank():
    # -- Constructeur --
    def __init__(self, nomBanque):
        self.m_id = uuid.uuid1()
        self.m_nom = nomBanque
        self.m_listUser = []
        self.m_listAccount = []
        self.m_listCredit = []

# -- Getter --
    def getId(self):
        return self.m_id

    def getNom(self):
        return self.m_nom

    def getUsers(self):
        return self.m_listUser

    def getAccounts(self):
        return self.m_listAccount

    def getCredits(self):
        return self.m_listCredit

    def addUser(self, user):
        self.m_listUser.append(user)

    def addAccount(self, account):
        self.m_listAccount.append(account)

    def addCredit(self, credit):
        self.m_listCredit.append(credit)

    def removeUser(self, userToDelete):
        for user in self.m_listUser:
            if user.checkLogin(userToDelete.getLogin()):
                self.m_listUser.remove(userToDelete)
                return True
        return False
